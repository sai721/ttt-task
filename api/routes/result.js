var express = require('express');
var router=express.Router()
var axios=require('axios')

function splitByWords (text) {
    // split string by spaces (including spaces, tabs, and newlines)
    var wordsArray = text.split(/\s+/);
    return wordsArray;
  }

  function createWordMap (wordsArray) {

    // create map for word counts
    var wordsMap = {};
    /*
      wordsMap = {
        'Oh': 2,
        'Feelin': 1,
        ...
      }
    */
    wordsArray.forEach(function (key) {
      if (wordsMap.hasOwnProperty(key)) {
        wordsMap[key]++;
      } else {
        wordsMap[key] = 1;
      }
    });
  
    return wordsMap;
  
  }
  
  
  function sortByCount (wordsMap) {
  
    // sort by count in descending order
    var finalWordsArray = [];
    finalWordsArray = Object.keys(wordsMap).map(function(key) {
      return {
        name: key,
        total: wordsMap[key]
      };
    });
  
    finalWordsArray.sort(function(a, b) {
      return b.total - a.total;
    });
  
    return finalWordsArray;
  
  }
router.get('/', function(req, res, next){
    axios.get('https://terriblytinytales.com/test.txt')
    .then(response => {
        let readData= response.data;
        let wordsArray = splitByWords(readData)
        let wordMap = createWordMap(wordsArray);
        let sortedData = sortByCount(wordMap);
        console.log(sortedData);
        res.send(sortedData);
    })
    .catch(err => {
        console.log(err);
    });
} )


module.exports= router